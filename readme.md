#Network Analyzer 

This is a Matlab class based on instrument toolbox. It provides way to connect to a network analyzer. Also, it includes functions to send, receive and query data from Network Analyzer.