classdef NetworkAnalyzer < handle
    properties
        name
        VISA_conn
    end
    
    methods
        function obj=NetworkAnalyzer()
           
        end
        
        function status=connect(obj)
            try
                % Find the VISA-GPIB object for NA at GPIB0 address 16 (default for Na).
                obj.VISA_conn = instrfind('Type', 'visa', 'RsrcName', 'GPIB0::16::0::INSTR', 'Tag', '');
                % Create the VISA-GPIB object if it does not exist
                % otherwise use the object that was found.
                if isempty(obj.VISA_conn)
                    obj.VISA_conn = visa('NI', 'GPIB0::16::0::INSTR');
                else
                    fclose(obj.VISA_conn);
                    obj.VISA_conn = obj.VISA_conn(1);
                end
                
                % Increase the buffer size (Provide Sufficient size)
                set(obj.VISA_conn, 'InputBufferSize', 524288);
                set(obj.VISA_conn, 'OutputBufferSize', 512);
                set(obj.VISA_conn, 'EOIMode', 'off');
                set(obj.VISA_conn, 'EOSMode', 'none');

                % Connect to Network Analyzer
                fopen(obj.VISA_conn);
            catch
                status=-1; %#ok<NASGU>
                disp('Something wrong with the VISA library drivers!');
                disp('Can not open the connection to Network Analyzer');
            end
            % Get Identification of Network Analyzer
            obj.name=obj.query('*IDN?');
            if obj.name<0
                disp('Network Analyzer not connected! Please check the connection');
                status=-1;
            else
                status=0;
            end
        end
        
        function status=disconnect(obj)
            try
                fclose(obj.VISA_conn);
                status=0;
            catch
                status=-1;
            end
        end
        
        function status=send(obj,cmd)
            try
                fprintf(obj.VISA_conn, '%s\r\n', cmd);
                status=0;
            catch
                status=-1;
            end
        end
        
        function data=recv(obj)
            try
                data = fscanf(obj.VISA_conn);
            catch
                data=-1;
            end
        end
        
        function data=query(obj,cmd)
           s=obj.send(cmd);
           if s==0
               data=obj.recv();
           else
               data=-2;
           end
        end
        
        function status=check_conn(obj)
            if obj.query('*IDN?')<0
                status=-1;
            else
                status=0;
            end
        end
        
        function clear_buffers(obj)
           flushinput(obj.VISA_conn);
           flushoutput(obj.VISA_conn);
        end
    end
        
end